import router from '@/router'
import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(sec), sec * 1000)
  })
}
instance.interceptors.request.use(
  async function (config) {
    const token = localStorage.getItem('access_token')
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    return config
  },
  function (eror) {
    if (401 === eror.response.status) {
      router.replace('/login')
    }
    return Promise.reject(eror)
  }
)
instance.interceptors.response.use(
  async function (res) {
    console.log('response interceptors')
    await delay(1)
    return res
  },
  function (error) {
    return Promise.reject(error)
  }
)
export default instance
